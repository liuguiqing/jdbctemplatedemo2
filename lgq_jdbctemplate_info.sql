/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2022-04-21 11:36:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `lgq_jdbctemplate_info`
-- ----------------------------
DROP TABLE IF EXISTS `lgq_jdbctemplate_info`;
CREATE TABLE `lgq_jdbctemplate_info` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(500) DEFAULT NULL COMMENT '题目',
  `texts` varchar(500) DEFAULT NULL COMMENT '内容',
  `createdTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of lgq_jdbctemplate_info
-- ----------------------------
INSERT INTO `lgq_jdbctemplate_info` VALUES ('1', 'xxx项目文件', '主要内容有xxxx', '2022-01-10 12:39:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('8', '新闻366633444', '内容-1', '2022-01-10 12:45:11');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('3', '项目1', '内容1', '2022-01-10 12:45:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('4', '项目2', '内容2', '2022-01-11 12:45:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('7', '项目', '内容', '2022-01-10 12:39:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('9', '项目16', '内容16', '2022-01-10 12:45:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('10', '项目26', '内容26', '2022-01-11 12:45:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('12', '新闻2', '我爱你中国2', '2022-01-10 12:45:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('13', '新闻2', '我爱你中国2', '2022-01-10 12:45:14');
INSERT INTO `lgq_jdbctemplate_info` VALUES ('14', '新闻2', '我爱你中国2', '2022-01-10 12:45:14');
